package com.example.androidlifecycle.data.models

import java.io.Serializable

data class SweetSerializableModel(
    override val name: String,
    override val surName: String,
    override val age: Int,
    override val isMarriage: Boolean
) : BaseParcelableModel(), Serializable
