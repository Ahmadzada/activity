package com.example.androidlifecycle.data.models

import android.os.Parcel
import android.os.Parcelable

data class SweetParcelableModel(
    override val name: String,
    override val surName: String,
    override val age: Int,
    override val isMarriage: Boolean
) : BaseParcelableModel(), Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(surName)
        parcel.writeInt(age)
        parcel.writeByte(if (isMarriage) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SweetParcelableModel> {
        override fun createFromParcel(parcel: Parcel): SweetParcelableModel {
            return SweetParcelableModel(parcel)
        }

        override fun newArray(size: Int): Array<SweetParcelableModel?> {
            return arrayOfNulls(size)
        }
    }
}
