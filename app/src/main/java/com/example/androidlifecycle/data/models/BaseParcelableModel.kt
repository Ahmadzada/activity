package com.example.androidlifecycle.data.models

abstract class BaseParcelableModel {
    abstract val name: String
    abstract val surName: String
    abstract val age: Int
    abstract val isMarriage: Boolean
}