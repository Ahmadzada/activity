package com.example.androidlifecycle.data.enums

enum class NameType {
    PARCELABLE, PARCELABLE_ANNOTATION, SERIALIZABLE,
}