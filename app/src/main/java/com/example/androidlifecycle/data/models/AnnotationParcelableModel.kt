package com.example.androidlifecycle.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

//import kotlinx.parcelize.Parcelize


@Parcelize
data class AnnotationParcelableModel(
    override val name: String,
    override val surName: String,
    override val age: Int,
    override val isMarriage: Boolean
) : BaseParcelableModel(), Parcelable