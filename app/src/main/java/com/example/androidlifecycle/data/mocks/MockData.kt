package com.example.androidlifecycle.data.mocks

import com.example.androidlifecycle.data.models.AnnotationParcelableModel
import com.example.androidlifecycle.data.models.SweetParcelableModel
import com.example.androidlifecycle.data.models.SweetSerializableModel

object MockData {
    fun getSweetParcelableModel() =
        SweetParcelableModel(
            name = " Sweet",
            surName = "Parcelable",
            age = 75,
            isMarriage = true
        )

    fun getAnnotationParcelableModel() =
        AnnotationParcelableModel(
            name = "Annotatioin",
            surName = "Parcelable annotation",
            age = 75,
            isMarriage = true
        )

    fun getSweetSerializableModel() =
        SweetSerializableModel(
            name = "Serializable",
            surName = "Serializable",
            age = 75,
            isMarriage = true
        )


}