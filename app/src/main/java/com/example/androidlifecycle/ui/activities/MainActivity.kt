package com.example.androidlifecycle.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.example.androidlifecycle.data.enums.NameType
import com.example.androidlifecycle.data.mocks.MockData
import com.example.androidlifecycle.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val intent = Intent(this@MainActivity, DetailActivity::class.java)
        val bundle = Bundle()

        val sweetParcelableModel = MockData.getSweetParcelableModel()
        val annotationParcelableModel = MockData.getAnnotationParcelableModel()
        val serializableModel = MockData.getSweetSerializableModel()


        bundle.putParcelable(NameType.PARCELABLE.name, sweetParcelableModel)
        bundle.putParcelable(NameType.PARCELABLE_ANNOTATION.name, annotationParcelableModel)
        bundle.putSerializable(NameType.SERIALIZABLE.name, serializableModel)
        intent.putExtras(bundle)

        binding.btnNode.setOnClickListener {
            node.launch(intent)
        }

    }

    val node =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                binding.btnNode.text = result.data?.extras?.getString("key")
            }
        }
}