package com.example.androidlifecycle.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.androidlifecycle.data.enums.NameType
import com.example.androidlifecycle.data.models.AnnotationParcelableModel
import com.example.androidlifecycle.data.models.SweetParcelableModel
import com.example.androidlifecycle.data.models.SweetSerializableModel
import com.example.androidlifecycle.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val bundle = intent.extras

        val parcelable =
            bundle?.getParcelable<SweetParcelableModel>(NameType.PARCELABLE.name) as SweetParcelableModel
        val annotation =
            bundle?.getParcelable<AnnotationParcelableModel>(NameType.PARCELABLE_ANNOTATION.name) as AnnotationParcelableModel
        val serializable =
            bundle?.getSerializable(NameType.SERIALIZABLE.name) as SweetSerializableModel


        binding.txtTest.text = """
            ${parcelable.name} ${parcelable.surName} ${parcelable.age}${parcelable.isMarriage} \n
            ${annotation.name} ${annotation.surName} ${annotation.age}${annotation.isMarriage} \n
            ${serializable.name} ${serializable.surName} ${serializable.age}${serializable.isMarriage} \n
        """.trimIndent()


        val bund = Bundle()
        bund.putString("key", "Salamlar")

        val myintent = Intent()
        myintent.putExtras(bund)

        setResult(Activity.RESULT_OK, myintent)

    }


}